/**
 * Created by luxiaohui on 16/3/8.
 */
exports.endWithError=function (res, msg, code) {
    var result = {
        code: code || -1,
        msg: msg
    };
    res.status(400);
    res.send(result);
    res.end();
};