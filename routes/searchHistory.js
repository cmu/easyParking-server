
var express = require('express');
var router = express.Router();
var searchHistoryDao=require('../dao/searchHistoryDao');
var errorHelper=require('../util/error_helper');

router.get('/query', function (req, res, next) {
    searchHistoryDao.queryAll(req,res,next);
});

router.get('/search', function (req, res, next) {
    //searchHistoryDao.queryAll(req,res,next);
    var params=req.query;
    console.log(params.user_id);
    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请先登录');
        return;
    }
    searchHistoryDao.searchHist(params.user_id,res,next);

});

router.get('/saveSearch', function (req, res, next) {
    //searchHistoryDao.queryAll(req,res,next);
    var params=req.query;
    console.log(params.user_id);
    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请先登录');
        return;
    }
    if(!params.search_value){
        errorHelper.endWithError(res,'请输入查询地址');
        return;
    }
    searchHistoryDao.saveSearch(params.user_id,params.search_value,res,next);

});


router.get('/saveParkSearch', function (req, res, next) {
    //searchHistoryDao.queryAll(req,res,next);
    var params=req.query;
    console.log(params.user_id);
    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请先登录');
        return;
    }
    if(!params.search_value){
        errorHelper.endWithError(res,'请输入查询停车场');
        return;
    }
    searchHistoryDao.saveParkSearch(params.user_id,params.search_value,res,next);
});

router.get('/savePOISearch', function (req, res, next) {
    //searchHistoryDao.queryAll(req,res,next);
    var params=req.query;
    console.log(params.user_id);
    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请先登录');
        return;
    }
    if(!params.search_value){
        errorHelper.endWithError(res,'请输入查询商家');
        return;
    }
    searchHistoryDao.savePOISearch(params.user_id,params.search_value,res,next);
});


router.delete('', function () {

    //TODO
});

module.exports = router;

