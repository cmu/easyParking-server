
var express = require('express');
var router = express.Router();
var parkComDao=require('../dao/parkComDao');
var errorHelper=require('../util/error_helper');

router.get('/queryCom', function (req, res, next) {
    var params=req.query;
    parkComDao.queryAll(params.P_Name,req,res,next);
});

router.get('/savePCom', function (req, res, next) {
    var params=req.query;
    console.log(params);
    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    parkComDao.savePCom(params.user_id,params.park_id,params.context,params.mark,params.time,params.user_nickname,res,next);

});


router.delete('', function () {

    //TODO
});

module.exports = router;

