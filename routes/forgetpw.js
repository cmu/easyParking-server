var express = require('express');
var router = express.Router();
var forgetpwDao=require('../dao/forgetpwDao');
var errorHelper=require('../util/error_helper');
/* GET users listing. */



router.get('/forgetpw', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.userType ){
        errorHelper.endWithError(res,'输入格式错误!');
        return;
    }
    if(!params.userName){
        errorHelper.endWithError(res,'用户名为空!');
        return;
    }
    forgetpwDao.forgetpw(params.userType,params.userName,params.userPwd,res,next);
});

router.delete('', function () {

    //TODO
});

module.exports = router;
