/**
 * Created by VGE on 2016/3/31.
 */
var express = require('express');
var router = express.Router();
var PCommentDao=require('../dao/PCommentDao');
var errorHelper=require('../util/error_helper');
/* GET users listing. */

  router.get('/PComment', function (req, res, next) {
    var params=req.query;
    console.log(params);
    if(!params.park_id){
        errorHelper.endWithError(res,'无此停车场数据！');
        return;
    }
      PCommentDao.Getdesc(params.park_id,res,next);
});


  router.get('/PComMark', function (req, res, next) {
    var params=req.query;
    console.log(params);
    if(!params.user_id&&!params.park_id ){
        errorHelper.endWithError(res,'无停车场评分信息！');
        return;
    }
      PCommentDao.AddPraise(params.user_id,params.park_id,res,next);
});


  router.get('/PLComment', function (req, res, next) {
    var params=req.query;
    console.log(params);
    if(!params.park_id ){
        errorHelper.endWithError(res,'错误的信息！');
        return;
    }
    PCommentDao.GetTimedesc(params.park_id,res,next);
});


  router.delete('', function () {
    //TODO
});

   module.exports = router;
