
var express = require('express');
var router = express.Router();
var PEditDao=require('../dao/PEditDao');
var errorHelper=require('../util/error_helper');

router.get('/queryPark', function (req, res, next) {
    var params=req.query;
    PEditDao.queryAll(params.P_Name,req,res,next);
});

router.get('/savePEdit', function (req, res, next) {
    var params=req.query;
    console.log(params);
    if(!params.P_Name){
        errorHelper.endWithError(res,'停车场名称不能为空');
        return;
    }
    PEditDao.savePEdit(params.P_Name,params.Type,params.Address,params.Tel,params.User_ID,params.Pay_not,params.Target_User,params.Price,params.Pay_Method,params.Total_Num,params.Adm,params.Height_limit,params.Pay_Time_Time,params.Free_Time_Time,res,next);

});

router.get('/updatePEdit', function (req, res, next) {
    var params=req.query;
    console.log(params);
    if(!params.P_Name){
        errorHelper.endWithError(res,'停车场名称不能为空');
        return;
    }
    PEditDao.updatePEdit(params.O_Name,params.P_Name,params.Type,params.Address,params.Tel,params.User_ID,params.Pay_not,params.Target_User,params.Price,params.Pay_Method,params.Total_Num,params.Adm,params.Height_limit,params.Pay_Time_Time,params.Free_Time_Time,res,next);

});

router.delete('', function () {

    //TODO
});

module.exports = router;

