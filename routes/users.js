var express = require('express');
var router = express.Router();
var userDao=require('../dao/userDao');
var errorHelper=require('../util/error_helper');
/* GET users listing. */

router.get('/', function (req, res, next) {
    userDao.queryAll(req,res,next);
});

router.get('/login', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.userName || !params.userPwd){
        errorHelper.endWithError(res,'用户名或密码不能为空');
        return;
    }
    userDao.login(params.userName,params.userPwd,res,next);

});

router.get('/queryByEmail', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.userEmail){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    userDao.queryByEmail(params.userEmail,res,next);

});

router.get('/queryById', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    userDao.queryById(params.user_id,res,next);

});

router.get('/queryByWechat', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.wechat_unionid){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    userDao.queryByWechat(params.nick_name,params.wechat_unionid,res,next);

});

router.get('/queryCollectOrNot', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    userDao.queryCollectOrNot(params.user_id,params.search_value,params.search_type,res,next);

});

router.get('/deleteCollect', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    userDao.deleteCollect(params.user_id,params.search_value,params.search_type,res,next);

});

router.get('/insertCollect', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.user_id){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    userDao.insertCollect(params.user_id,params.search_value,params.search_type,res,next);

});
router.get('/insertWechatUser', function (req, res, next) {
    var params=req.query;
    console.log(params);

    if(!params.nick_name){
        errorHelper.endWithError(res,'用户未登录，请登录');
        return;
    }
    userDao.insertWechatUser(params.nick_name,params.wechat_unionid,res,next);

});

router.delete('', function () {

    //TODO
});

module.exports = router;
