
var express = require('express');
var router = express.Router();
var registerDao=require('../dao/registerDao');
var errorHelper=require('../util/error_helper');

router.get('/query', function (req, res, next) {
    registerDao.queryAll(req,res,next);
});

router.get('/register', function (req, res, next) {
    //registerDao.queryAll(req,res,next);
    var params=req.query;
    console.log(params);
    if(!params.userName || !params.userPwd){
        errorHelper.endWithError(res,'用户名或密码不能为空');
        return;
    }
  registerDao.register(params.userName,params.userPwd,res,next);

});


router.delete('', function () {

    //TODO
});

module.exports = router;

