/**
 * Created by VGE on 2016/3/31.
 */
var express = require('express');
var router = express.Router();
var ParkdetailDao=require('../dao/ParkdetailDao');
var errorHelper=require('../util/error_helper');
/* GET users listing. */

router.get('/Parkdetail', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.lat && !params.lng ){
        errorHelper.endWithError(res,'无停车场信息！');
        return;
    }

    ParkdetailDao.Getdetail(params.lat,params.lng,res,next);
});



module.exports = router;
