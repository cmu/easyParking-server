/**
 * Created by VGE on 2016/3/31.
 */
var express = require('express');
var router = express.Router();
var ParkdescDao=require('../dao/ParkdescDao');
var errorHelper=require('../util/error_helper');
/* GET users listing. */

  router.get('/Parkdesc', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.name ){
        errorHelper.endWithError(res,'无此停车场信息！');
        return;
    }

    ParkdescDao.Getdesc(params.name,res,next);
});

router.get('/queryPark', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.parkID ){
        errorHelper.endWithError(res,'无此停车场信息！');
        return;
    }

    ParkdescDao.queryByID(params.parkID,res,next);
});

  router.get('/Parkdescs', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.park_id ){
        errorHelper.endWithError(res,'无停车场评分信息！');
        return;
    }

    ParkdescDao.Getmark(params.park_id,res,next);
});
router.get('/offStreetCharge', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.park_id ){
        errorHelper.endWithError(res,'无一般停车场收费信息！');
        return;
    }

    ParkdescDao.OffCharge(params.park_id,res,next);
});
router.get('/onStreetCharge', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.park_id ){
        errorHelper.endWithError(res,'无一般停车场收费信息！');
        return;
    }

    ParkdescDao.OnCharge(params.park_id,res,next);
});

router.get('/ParkSpace', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.park_id ){
        errorHelper.endWithError(res,'无一般停车场收费信息！');
        return;
    }

    ParkdescDao.OnParkSpace(params.park_id,res,next);
});

router.get('/ParkEntity', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.park_id ){
        errorHelper.endWithError(res,'无一般停车场收费信息！');
        return;
    }

    ParkdescDao.OffParkEntity(params.park_id,res,next);
});

router.get('/ParkRealtime', function (req, res, next) {

    var params=req.query;
    console.log(params);

    if(!params.park_id ){
        errorHelper.endWithError(res,'无一般停车场收费信息！');
        return;
    }

    ParkdescDao.ParkRealtimeInfo(params.park_id,res,next);
});

  router.delete('', function () {

    //TODO
});

   module.exports = router;
