/**
 * Created by luxiaohui on 16/3/24.
 */
// conf/db.js
// MySQL数据库联接配置
module.exports = {
    mysql: {
        host: '172.21.212.196',
        user: 'root',
        password: 'newpass',
        database:'upark', // 前面建的user表位于这个数据库中
        port: 3306
    }
};