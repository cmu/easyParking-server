/**
 * Created by caojie on 16/3/25.
 */


var mysql = require('mysql');
var $conf = require('../conf/db');
var $sql = require('./searSqlMapping');
var format = require('string-format');


var pool = mysql.createPool($conf.mysql);


var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.status(400);
        res.json({
            code: '-1',
            msg: '操作失败'
        });
    } else if (ret.code == -1) {
        res.status(400);
        res.json(ret);
    } else {
        res.json(ret);

    }
};

module.exports = {

    queryAll: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query($sql.queryAll, function (err, result) {
                jsonWrite(res, result);
                connection.release();
            });
        });
    },
    searchHist: function(userName, res, next){
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
          var sql=$sql.queryByUser+userName+' order by search_id desc';
            console.log(sql);
            connection.query(sql, function (err, result) {
                    jsonWrite(res, result);
                });
               connection.release();
            });

    },

    saveSearch: function(user_id,search_value, res, next){
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql=$sql.insert+user_id+',\''+search_value+'\')';
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '保存地址搜索纪录成功'
                    };
                }
                jsonWrite(res, result);
            });
            connection.release();
        });

    },

    saveParkSearch: function(userName,ParkName, res, next){
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql=$sql.insertP+userName+',\''+ParkName+'\')';
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '保存停车场搜索纪录成功'
                    };
                }
                jsonWrite(res, result);
            });
            connection.release();
        });
    },

    savePOISearch: function(userName,poi, res, next){
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql=$sql.insertPOI+userName+',\''+poi+'\')';
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '保存POI搜索纪录成功'
                    };
                }
                jsonWrite(res, result);
            })
            connection.release();
        });
    },
};
