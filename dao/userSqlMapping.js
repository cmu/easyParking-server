/**
 * Created by luxiaohui on 16/3/25.
 */
// dao/userSqlMapping.js
// CRUD SQL语句
var tableName = 'user_info';
var collectTable='user_collection_info';
var user = {
    queryAll: 'select * from '+tableName+'',
    queryByCondition: 'select * from '+tableName+' where ',
    queryByEmail: 'select * from '+tableName+' where email=',
    queryById: 'select * from '+tableName+' where user_id=',
    queryByWechatUnionid: 'select * from '+tableName+' where wechat_unionid=',
    insertWechatUser:'insert into '+tableName+' (nick_name,wechat_unionid) values(',

    queryCollectOrNot:'select *from '+collectTable+' where user_id=',
    deleteCollect:'delete from '+collectTable+' where user_id= ',
    insertCollect:'insert into '+collectTable+' (user_id,search_value,search_type) values(',
};

module.exports = user;