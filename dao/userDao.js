/**
 * Created by luxiaohui on 16/3/24.
 */
// dao/userDao.js
// 实现与MySQL交互
var mysql = require('mysql');
var $conf = require('../conf/db');
var $sql = require('./userSqlMapping');
var format = require('string-format');

// 使用连接池，提升性能
var pool = mysql.createPool($conf.mysql);

// 向前台返回JSON方法的简单封装
var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.status(400);
        res.json({
            code: '-1',
            msg: '操作失败'
        });
    } else if (ret.code == -1) {
        res.status(400);
        res.json(ret);
    } else {
        res.json(ret);

    }
};

module.exports = {

    queryAll: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query($sql.queryAll, function (err, result) {
                jsonWrite(res, result);
                connection.release();
            });
        });
    },
    queryByEmail: function ( userEmail, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {}', $sql.queryByEmail, '\''+ userEmail+'\'');

            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '未查到该用户'});
                }
                else {
                    jsonWrite(res, result[0]);
                }
                console.log(result);
                connection.release();
            });
        })
    },
    queryById: function ( user_id, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {}', $sql.queryById, '\''+ user_id+'\'');

            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '未查到该用户'});
                }
                else {
                    jsonWrite(res, result[0]);
                }
                console.log(result);
                connection.release();
            });
        })
    },
    //查找微信用户纪录，若没有则新建
    queryByWechat: function ( nick_name,wechat_unionid, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {}', $sql.queryByWechatUnionid, '\''+ wechat_unionid+'\'');

            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    //jsonWrite(res, {code: -1, msg: '未查到该用户'});
                    var newsql = format('{} {} , {}  ', $sql.insertWechatUser,'\''+nick_name+'\'','\''+wechat_unionid+'\')');
                    console.log(newsql);
                    connection.query(newsql, function (err, result1) {
                        jsonWrite(res,  result1);//写出保存结果
                    });
                }
                else {
                    jsonWrite(res, result[0]);
                }
                console.log(result);
                connection.release();
            });
        })
    },
    //添加收藏
    insertWechatUser:function(nick_name,wechat_unionid,res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} , {}  ', $sql.insertWechatUser,'\''+nick_name+'\'','\''+wechat_unionid+'\')');
            console.log(sql);
            connection.query(sql, function (err, result1) {
                jsonWrite(res,  result1);//写出保存结果
                connection.release();
            });
        });
    },

    login: function ( userName, userPwd, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }

            var sql = format('{} {}={}  limit 1', $sql.queryByCondition,'email', '\''+ userName+'\'');

            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '未查到该用户'});
                }
                if (result[0].user_pwd != userPwd) {
                    jsonWrite(res, {code: -1, msg: '用户密码错误'});

                } else {
                    jsonWrite(res, result[0]);
                }
                console.log(result);
                connection.release();
            });
        })
    },
    //查询用户是否已收藏
    queryCollectOrNot:function(user_id,search_value,search_type,res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} and search_value={} and search_type={} ', $sql.queryCollectOrNot,'\''+user_id+'\'','\''+search_value+'\'','\''+search_type+'\'');
            console.log(sql);
            connection.query(sql, function (err, result1) {
                if (!result1 || result1.length == 0) {
                    jsonWrite(res, {code: -1, msg: '无收藏的信息！'});
                }
                else{
                    jsonWrite(res,  result1);//写出保存结果
                    console.log( result1);
                };
                connection.release();
            });
        });
    },
    //删除收藏
    deleteCollect:function(user_id,search_value,search_type,res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} and search_value={} and search_type={} ', $sql.deleteCollect,'\''+user_id+'\'','\''+search_value+'\'','\''+search_type+'\'');
            console.log(sql);
            connection.query(sql, function (err, result1) {
                jsonWrite(res,  result1);//写出保存结果
                connection.release();
            });
        });
    },
    //添加收藏
    insertCollect:function(user_id,search_value,search_type,res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} , {} , {} ', $sql.insertCollect,'\''+user_id+'\'','\''+search_value+'\'','\''+search_type+'\')');
            console.log(sql);
            connection.query(sql, function (err, result1) {
                jsonWrite(res,  result1);//写出保存结果
                connection.release();
            });
        });
    },

};
