/**
 * Created by VGE on 2016/3/31.
 */


var mysql = require('mysql');
var $conf = require('../conf/db');
var $sql = require('./Parkdescsql');
var format = require('string-format');


var pool = mysql.createPool($conf.mysql);


var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.status(400);
        res.json({
            code: '-1',
            msg: '未定义'
        });
    } else if (ret.code == -1) {
        res.status(400);
        res.json(ret);
    } else {
        res.json(ret);

    }
};

module.exports = {
    add: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            var param = req.query || req.params;

            // 'INSERT INTO user(id, name, age) VALUES(0,?,?)',
            connection.query($sql.insert, [param.name, param.age], function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '无结果'
                    };
                }

                jsonWrite(res, result);

                connection.release();
            });
        });
    },

    queryAll: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query($sql.queryAll, function (err, result) {
                jsonWrite(res, result);
                connection.release();
            });
        });
    },

    //获取停车场详情信息
    Getdesc: function (name, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }

            //查询停车场描述信息
            var sql = format('{} {} ', $sql.queryDesc,'\''+name+'\'');   console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '无停车场详细信息！'});
                }
                else{
                    jsonWrite(res, result);
                };

              //  console.log(result);
                  connection.release();
            });
        })
    },
    //获取停车场详情信息
    queryByID: function (parkID, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} ', $sql.queryByID,'\''+parkID+'\'');
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '无停车场详细信息！'});
                }
                else{
                    jsonWrite(res, result);
                };
                connection.release();
            });
        })
    },


    //获取评价的平均分
    Getmark: function (park_id, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var Msql = format('{} {} ', $sql.queryMark,'\''+park_id+'\'');   console.log(Msql);
                    connection.query(Msql, function (err, result1) {
                        if (!result1 || result1.length == 0) {
                            jsonWrite(res, {code: -1, msg: '无停车场评分数据！'});
                        }
                        else{
                            jsonWrite(res,  result1);//写出得到的平均分值
                            console.log( result1);
                        };
                        connection.release();
                    });
            });
    },
    //获取一般停车场收费信息
    OffCharge: function (parkID, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} ', $sql.queryOffCharge,'\''+parkID+'\'');
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                  jsonWrite(res, {code: -1, msg: '无该一般停车场收费信息！'});
                }
                else{
                    jsonWrite(res, result);
                };
                connection.release();
            });
        })
    },
    //获取街边停车场收费信息
    OnCharge: function (parkID, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} ', $sql.queryOnCharge,'\''+parkID+'\'');
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                   jsonWrite(res, {code: -1, msg: '无街边停车场收费信息！'});
                }
                else{
                    jsonWrite(res, result);
                };
                connection.release();
            });
        })
    },
    //获取街边停车场停车位信息
    OnParkSpace: function (parkID, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} ', $sql.queryParkSpace,'\''+parkID+'\'');
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '无街边停车场停车位信息！'});
                }
                else{
                    jsonWrite(res, result);
                };
                connection.release();
            });
        })
    },
    //获取非街边停车场出入口信息
    OffParkEntity: function (parkID, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} ', $sql.queryParkEntity,'\''+parkID+'\'');
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '无非街边停车场出入口信息！'});
                }
                else{
                    jsonWrite(res, result);
                };
                connection.release();
            });
        })
    },
    //获取停车场动态信息
    ParkRealtimeInfo: function (parkID, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} ', $sql.queryParkRealtimeInfo,'\''+parkID+'\'');
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                   jsonWrite(res, {code: -1, msg: '无停车场动态信息！'});
                }
                else{
                    jsonWrite(res, result);
                };
                connection.release();
            });
        })
    },
}