/**
  *Created by caojie on 16/3/25.
 **/


var tableName = 'p_user_data';
var user = {
    insert: 'INSERT INTO '+tableName,
    update: 'update '+tableName+' set name=?, age=? where id=?',
    delete: 'delete from '+tableName+' where id=?',
    queryById: 'select * from '+tableName+' where id=?',
    queryAll: 'select * from '+tableName+'',
    queryByCondition: 'select * from '+tableName+' where '
};

module.exports = user;
