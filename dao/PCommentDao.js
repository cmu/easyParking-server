/**
 * Created by VGE on 2016/3/31.
 */


var mysql = require('mysql');
var $conf = require('../conf/db');
var $sql = require('./PComsql');
var format = require('string-format');


var pool = mysql.createPool($conf.mysql);


var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.status(400);
        res.json({
            code: '-1',
            msg: '未定义'
        });
    } else if (ret.code == -1) {
        res.status(400);
        res.json(ret);
    } else {
        res.json(ret);

    }
};

module.exports = {
    add: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            var param = req.query || req.params;

            // 'INSERT INTO user(id, name, age) VALUES(0,?,?)',
            connection.query($sql.insert, [param.name, param.age], function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '无结果'
                    };
                }

                jsonWrite(res, result);

                connection.release();
            });
        });
    },

    queryAll: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query($sql.queryAll, function (err, result) {
                jsonWrite(res, result);
                connection.release();
            });
        });
    },

    //按点赞降序排列评价
    Getdesc: function (park_id, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }

            var sql = format('{} {} order by praise desc  ', $sql.queryCom,'\''+park_id+'\'');   console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '无停车场数据！'});
                }
                else{
                    jsonWrite(res, result);
               };
                  connection.release();
            });
        })
    },

    //增加点赞数据
    AddPraise: function (user_id,park_id,res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} and park_id={} ', $sql.queryMark, '\''+user_id+'\'','\''+park_id+'\'');
            console.log(sql);
            connection.query(sql, function (err, result1) {
                if (!result1 || result1.length == 0) {
                    jsonWrite(res, {code: -1, msg: '点赞出错！'});
                }
                else{
                    jsonWrite(res,  result1);//写出保存结果
                    console.log( result1);
                };
                connection.release();

            });
        });
    },

    //按时间降序排列评价
    GetTimedesc: function (park_id, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql = format('{} {} order by time desc  ', $sql.queryComasTime ,'\''+park_id+'\'');
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '无停车场数据！'});
                }
                else{
                    jsonWrite(res, result);
                };
                connection.release();
            });
        })
    },

}