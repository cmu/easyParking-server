/**
 * Created by VGE on 2016/3/27.
 */


var mysql = require('mysql');
var $conf = require('../conf/db');
var $sql = require('./fgtpwsql');
var format = require('string-format');


var pool = mysql.createPool($conf.mysql);


var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.status(400);
        res.json({
            code: '-1',
            msg: '未定义'
        });
    } else if (ret.code == -1) {
        res.status(400);
        res.json(ret);
    } else {
        res.json(ret);

    }
};

module.exports = {

    queryAll: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query($sql.queryAll, function (err, result) {
                jsonWrite(res, result);
                connection.release();
            });
        });
    },
    forgetpw: function (userType, userName, userPwd, res, next) {
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }

            var sql = format('{} {}={}  limit 1', $sql.queryByCondition, 'email' ,'\''+ userName+'\'');console.log(sql);
            connection.query(sql, function (err, result) {
                if (!result || result.length == 0) {
                    jsonWrite(res, {code: -1, msg: '用户名为空'});
                }
               else {
                  //  jsonWrite(res, result[0]);
                    //修改密码
                    var istsql=format('{}{} where {}={} limit 1',$sql.update,'\''+userPwd+'\'', 'email' ,'\''+ userName+'\'');
                        console.log(istsql);
                        connection.query(istsql,function(err,result){
                                jsonWrite(res,{msg:'修改成功'})
                            }
                        )

                }
             //   console.log(result);
                connection.release();
            });
        })
    },

}