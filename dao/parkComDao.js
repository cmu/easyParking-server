/**
 * Created by caojie on 16/3/25.
 */


var mysql = require('mysql');
var $conf = require('../conf/db');
var $sql = require('./pComSqlMapping');
var format = require('string-format');


var pool = mysql.createPool($conf.mysql);


var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.status(400);
        res.json({
            code: '-1',
            msg: '操作失败'
        });
    } else if (ret.code == -1) {
        res.status(400);
        res.json(ret);
    } else {
        res.json(ret);

    }
};

module.exports = {

    queryAll: function (req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query($sql.queryAll, function (err, result) {
                jsonWrite(res, result);
                connection.release();
            });
        });
    },
    savePCom: function(user_id,park_id,mark,context,time,user_nickname, res, next){
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql=$sql.insert+'\''+user_id+'\','+'\''+park_id+'\','+'\''+context+'\','+'\''+mark+'\','+'\''+0+'\','+'\''+time+'\','+'\''+user_nickname+'\')';
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '保存停车场评论成功'
                    };
                }
                jsonWrite(res, result);
               connection.release();
            });
        })
    }
};
