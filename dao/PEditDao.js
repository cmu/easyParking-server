/**
 * Created by caojie on 16/3/25.
 */


var mysql = require('mysql');
var $conf = require('../conf/db');
var $sql = require('./PEditSqlMapping');
var format = require('string-format');


var pool = mysql.createPool($conf.mysql);


var jsonWrite = function (res, ret) {
    if (typeof ret === 'undefined') {
        res.status(400);
        res.json({
            code: '-1',
            msg: '操作失败'
        });
    } else if (ret.code == -1) {
        res.status(400);
        res.json(ret);
    } else {
        res.json(ret);

    }
};

module.exports = {
    queryAll: function (P_Name,req, res, next) {
        pool.getConnection(function (err, connection) {
            var sql=$sql.queryAll+'\''+P_Name+'\'';
            connection.query(sql, function (err, result) {
                jsonWrite(res, result);
                connection.release();
            });
        });
    },
    savePEdit: function(P_Name,Type,Address,Tel,User_ID,Pay_not,Target_User,Price,Pay_Method,Total_Num,Adm,Height_limit,Pay_Time_Time,Free_Time_Time, res, next){
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql=$sql.insert+'\''+P_Name+'\''+',\''+Type+'\''+',\''+Address+'\''+',\''+Tel+'\''+',\''+User_ID+'\''+',\''+Pay_not+'\''+',\''+Target_User+'\''+',\''+Price+'\''+',\''+Pay_Method+'\''+',\''+Total_Num+'\''+',\''+Adm+'\''+',\''+Height_limit+'\''+',\''+Pay_Time_Time+'\''+',\''+Free_Time_Time+'\')';
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '保存停车场编辑信息成功'
                    };
                }
                jsonWrite(res, result);
            });
            connection.release();
        });
    },
    updatePEdit: function(O_Name,P_Name,Type,Address,Tel,User_ID,Pay_not,Target_User,Price,Pay_Method,Total_Num,Adm,Height_limit,Pay_Time_Time,Free_Time_Time, res, next){
        pool.getConnection(function (err, connection) {
            if (err) {
                jsonWrite(res, {code: -1, msg: err});
                return;
            }
            var sql=$sql.update+'P_Name=\''+P_Name+'\''+',Type=\''+Type+'\''+',Address=\''+Address+'\''+',Tel=\''+Tel+'\''+',User_ID=\''+User_ID+'\''+',Pay_not=\''+Pay_not+'\''+',Target_User=\''+Target_User+'\''+',Price=\''+Price+'\''+',Pay_Method=\''+Pay_Method+'\''+',Total_Num=\''+Total_Num+'\''+',Adm=\''+Adm+'\''+',Height_limit=\''+Height_limit+'\''+',Pay_Time_Time=\''+Pay_Time_Time+'\''+',Free_Time_Time=\''+Free_Time_Time+'\' Where P_Name='+'\''+O_Name+'\'';
            console.log(sql);
            connection.query(sql, function (err, result) {
                if (result) {
                    result = {
                        code: 200,
                        msg: '保存停车场信息更新成功'
                    };
                }
                jsonWrite(res, result);
            });
            connection.release();
        });
    },


};
